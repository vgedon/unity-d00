﻿using UnityEngine;
using System.Collections;

public class Ball : MonoBehaviour
{
	private Vector3 	innerForce = Vector3.zero;
	private Vector3 	direction;
	private Vector3 	start_position;
	private int			score = -15;
	private bool		flag = false;

	public GameObject	Hole;

	// Use this for initialization
	void Start ()
	{
		direction = new Vector3 (0f, 1f, 0f);
		start_position = transform.localPosition;
		innerForce = Vector3.zero;
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (innerForce.y > 0f) {
			transform.Translate (direction * innerForce.y);
			innerForce -= new Vector3 (0, 0.2f);
		} else {
			innerForce = Vector3.zero;
		}
		if (innerForce.y > 0f && (transform.position.y > 8.5f || transform.position.y < -8.5f)) {
			direction = direction * -1f;
			if (transform.position.y > 8.5f)
				transform.position = new Vector3 (0f, 8.5f, 0f);
			else
				transform.position = new Vector3 (0f, -8.5f, 0f);
		} else if (flag && innerForce == Vector3.zero) {
			score += 5;
			flag = false;
			if ((direction.y > 0 && this.transform.position.y > Hole.transform.position.y)
				|| (direction.y < 0 && this.transform.position.y < Hole.transform.position.y)) {
				direction = direction * -1f;
			}
			Debug.Log ("score: " + score);
		}
	}
	void FixedUpdate ()
	{
		if (innerForce.y <= 0.3f && transform.position.y < Hole.transform.position.y + 0.3f
			&& transform.position.y > Hole.transform.position.y - 0.3f) {
			score += 5;
			Debug.Log ("WIN ! score: " + score);
			score = -15;
			flag = false;
			innerForce = Vector3.zero;
			transform.position = start_position;
			direction = new Vector3 (0f, 1f, 0f);
		}
	}
	
	public void SetInnerForce (Vector3 force)
	{
		innerForce = force;
		flag = true;
	}
	
	public Vector3 GetInnerForce ()
	{
		return (innerForce);
	}

	public Vector3 GetDirection ()
	{
		return (direction);
	}
}
