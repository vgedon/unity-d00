﻿using UnityEngine;
using System.Collections;

public class Club : MonoBehaviour
{
	private float 		force = 0;
	private Vector3		offset;
	private bool		change_pos_flag = false;

	public Ball			ball;

	// Use this for initialization
	void Start ()
	{
		offset = this.transform.position - ball.transform.position;
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (Input.GetKey (KeyCode.Space)) {
			force += 0.1f;
			transform.Translate (new Vector3 (0f, -0.02f * ball.GetDirection ().y, 0f));
		} else if (force > 0) {
			transform.Translate (new Vector3 (0f, force * ball.GetDirection ().y, 0f));
			ball.SetInnerForce (new Vector3 (0f, force, 0f));
			force = 0;
			change_pos_flag = true;
		}
	}

	void LateUpdate ()
	{
		if (change_pos_flag && ball.GetInnerForce () == Vector3.zero) {
			change_pos_flag = false;
			if (ball.GetDirection ().y > 0) {
				transform.position = ball.transform.position + offset;
			} else {
				transform.position = ball.transform.position + offset - ball.GetDirection ();
			}
		}
	}
}