﻿using UnityEngine;
using System.Collections;

public class Bird : MonoBehaviour
{
	private float 			dist = ((17 + 26) / 2) / 10;
	private Vector3			innerForce;
	private int				score = 0;
	private bool[]			flag = {false, false};

	public Pipe[]			pipes;
	// Bird is 17 x 12

	// Use this for initialization
	void Start ()
	{
		innerForce = Vector3.zero;
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (Input.GetKeyDown (KeyCode.Space)) {
			innerForce = Vector3.up * 0.15f;
		}
		if (transform.position.y > 4.6f && innerForce.y > 0f) {
			innerForce = Vector3.zero;
		}
		transform.Translate (innerForce);
		innerForce -= Vector3.up * 0.01f;


		foreach (Pipe pipe in pipes) {
			float distance = pipe.transform.position.x - transform.position.x;
			if (distance < dist && distance > -dist) {
				flag [(pipe.name == "pipe1") ? 1 : 0] = true;
				if (transform.position.y < pipe.transform.position.y - 1.62f
					|| transform.position.y > pipe.transform.position.y + 1.25f) {
					GameOver ();
				}
			} else if (flag [(pipe.name == "pipe1") ? 1 : 0]) {
				score += 5;
				flag [(pipe.name == "pipe1") ? 1 : 0] = false;
			}
		}
		if (transform.position.y < -6f) {
			GameOver ();
		}
	}

	private void GameOver ()
	{
		Debug.Log ("Game Over !!");
		Debug.Log ("Score: " + score);
		Debug.Log ("Time: " + Mathf.RoundToInt (Time.time) + "s");
		Destroy (this);
		foreach (Pipe pipe in pipes) {
			Destroy (pipe.gameObject);
		}
	}
}
