﻿using UnityEngine;
using System.Collections;

public class Pipe : MonoBehaviour
{
	public float		speed;
	// Pipe is 26 x 256

	// Use this for initialization
	void Start ()
	{
		speed = (speed <= 0f ? 0.05f : speed);
	}
	
	// Update is called once per frame
	void Update ()
	{
		transform.Translate (-speed, 0f, 0f);
		if (transform.position.x < -10f) {
			transform.position = new Vector3 (10f, transform.position.y, 0f);
		}
	}
}
