﻿using UnityEngine;
using System.Collections;

public class balloon : MonoBehaviour
{

	private float time;
	private float stamina;

	public float inactivityTimeBeforeLoose = 10;
	public int maxStamina = 100;
	public float cost = 10;
	// Use this for initialization
	void Start ()
	{
		transform.localScale = new Vector3 (0.2f, 0.2f);
		stamina = maxStamina;
		time = inactivityTimeBeforeLoose;
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (Input.GetKeyDown (KeyCode.Space) && stamina > cost) {
			transform.localScale += new Vector3 (0.2f, 0.2f);
			stamina -= cost;
			time = inactivityTimeBeforeLoose;
		} else {
			stamina += cost / maxStamina;
			stamina = stamina > maxStamina ? maxStamina : stamina;
			time -= Time.deltaTime;

			// deflating the baloon isn't a requested behavior
			if (transform.localScale.x >= 0.2f) {
				transform.localScale -= new Vector3 (0.001f, 0.001f);
			}
		}
	}
	void LateUpdate ()
	{
		if (transform.localScale.x >= 6 || time <= 0) {
			Destroy (gameObject);
		}
	}

	void OnDestroy ()
	{
		Debug.Log ("Balloon life time: " + Mathf.RoundToInt (Time.timeSinceLevelLoad) + "s");

	}
}
