﻿using UnityEngine;
using System.Collections;

public class Cube : MonoBehaviour {

	private float Speed;
	public GameObject target;
	public KeyCode keycode;
	private float distance;

	// Use this for initialization
	void Start () {
		Speed = Random.Range (0.05f, 0.3f);
	}
	
	// Update is called once per frame
	void Update () {
		transform.Translate(new Vector3 (0f, - Speed));
		if (transform.localPosition.y < -5) {
			Destroy (gameObject);
		}
	}

	void LateUpdate () {;
		if (Input.GetKeyDown(keycode)) {
			distance = target.transform.localPosition.y < transform.localPosition.y ? transform.localPosition.y - target.transform.localPosition.y : target.transform.localPosition.y - transform.localPosition.y ;
			Debug.Log ("Precision: "+distance);
			Destroy (gameObject);
		}
	}
}
