﻿using UnityEngine;
using System.Collections;

public class CubeSpawner : MonoBehaviour {

	public GameObject prefab;
	private float interval;
	public float max;
	public float min;
	public GameObject target;
	private GameObject instance;

	// Use this for initialization
	void Start () {
		interval = Random.Range(min, max);
	}
	
	// Update is called once per frame
	void Update () {
		if (interval <= 0 && !instance) {
			instance = Instantiate (prefab);
			instance.GetComponent<Cube>().target = target;
			interval = Random.Range (min, max);
		} else {
			interval -= Time.deltaTime;
		}
	}
}
