﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour
{
	private float		speed;

	public KeyCode		up;
	public KeyCode		down;

	// Use this for initialization
	void Start ()
	{
		speed = 0.2f;
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (Input.GetKey (up) && transform.position.y < (3.5f - speed)) {
			transform.Translate (Vector3.up * speed);
		} else if (Input.GetKey (down) && transform.position.y > (-3.5f + speed)) {
			transform.Translate (Vector3.down * speed);
		}
	}
}
