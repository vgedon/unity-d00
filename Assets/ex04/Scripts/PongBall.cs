﻿using UnityEngine;
using System.Collections;

public class PongBall : MonoBehaviour
{

	private Vector3			start_position;
	private Vector3			direction;
	private float			speed = 0.1f;
	
	public	Player			playerLeft;
	public	Player			playerRight;
	
	private int				scorePlayerLeft = 0;
	private int				scorePlayerRight = 0;
	private bool			colision_test_flag = false;

	// Use this for initialization
	void Start ()
	{
		start_position = transform.position;
		direction = new Vector3 (Random.Range (0f, 2f) > 1f ? 1f : -1f, Random.Range (speed, 1f));
	}
	
	// Update is called once per frame
	void Update ()
	{
		transform.Translate (direction * speed);
		if (transform.position.x < 7.5f && transform.position.x > -7.5f) {
			colision_test_flag = false;
		}
	}

	void FixedUpdate ()
	{
		if ((transform.position.y >= 4.25f && direction.y > 0f)
			|| (transform.position.y <= -4.25f && direction.y < 0f)) {
			direction.y = -direction.y;
		}

		if (transform.position.x >= 7.5f && direction.x > 0f && !colision_test_flag) {
			colision_test_flag = true;
			if (transform.position.y <= playerRight.transform.position.y + 1.1f 
				&& transform.position.y >= playerRight.transform.position.y - 1.1f) {
				direction.x = -direction.x;
			}
		} else if (transform.position.x <= -7.5f && direction.x < 0f && !colision_test_flag) {
			colision_test_flag = true;
			if (transform.position.y <= playerLeft.transform.position.y + 1.1f 
				&& transform.position.y >= playerLeft.transform.position.y - 1.1f) {
				direction.x = -direction.x;
			}
		}
	}

	void LateUpdate ()
	{
		if (transform.position.x >= 8.5f) {
			scorePlayerLeft++;
			SetWin ();
		} else if (transform.position.x <= -8.5f) {
			scorePlayerRight++;
			SetWin ();
		}
	}

	private void SetWin ()
	{
		colision_test_flag = false;
		transform.position = start_position;
		Debug.Log ("Player 1: " + scorePlayerLeft + " | Player 2: " + scorePlayerRight);
		direction = new Vector3 (Random.Range (0f, 2f) > 1f ? 1f : -1f, Random.Range (speed, 1f));
	}
}
